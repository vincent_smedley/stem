#include <Servo.h>

#define RIGHT 1
#define LEFT-1

Servo leftServo;
Servo rightServo;

const byte ledPin = 13;
const byte buttonPin = 9;

const byte power = 250;

void foward()
{
  leftServo. writeMicroseconds(1500 - power);
  rightServo. writeMicroseconds(1500 + power);
}

void stop(int time = 500)
{
  leftServo.writeMicroseconds(1500);
  rightServo.writeMicroseconds(1500);
  delay(time);
}

void turn(int direction, int degrees)
{
  leftServo.writeMicroseconds(1500 + power * direction);
  rightServo.writeMicroseconds(1500 + power * direction);
  delay(degrees * 5.5);
  stop();


}

void forwardTime(unsigned int time)
{
  foward();
  delay(time);
  stop();
}

void setup()
{
  leftServo.attach(6);
  rightServo.attach(5);

  pinMode (ledPin, OUTPUT);
  pinMode (buttonPin, INPUT_PULLUP);

  while (digitalRead(buttonPin))
  {
  }
  
  for (int x = 0; x < 4; x++)
  {
    forwardTime(2000);
    turn(LEFT, 90);
  }

}

void loop()
{
  digitalWrite(ledPin, HIGH);
}